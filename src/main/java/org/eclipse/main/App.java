package org.eclipse.main;

import org.eclipse.model.Adresse;
import org.eclipse.model.Personne;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class App 
{
    public static void main( String[] args )
    {
    	
    	Adresse adresse1 = new Adresse(); 
    	adresse1.setRue("berger"); 
    	adresse1.setCodePostal("14015"); 
    	adresse1.setVille("Menton");
//    	
//    	Adresse adresse2 = new Adresse(); 
//    	adresse2.setRue("gerard"); 
//    	adresse2.setCodePostal("13037"); 
//    	adresse2.setVille("Valbonne");
//    	
//    	Adresse adresse3 = new Adresse(); 
//    	adresse3.setRue("popo"); 
//    	adresse3.setCodePostal("20035"); 
//    	adresse3.setVille("Nissa");
    	
    	Personne personne = new Personne();  
    	personne.setId(1);
    	personne.setNom("nanono"); 
    	personne.setPrenom("Pidra");
    	personne.setAdresse(adresse1);
//    	personne.addAdresse(adresse1);
//    	personne.addAdresse(adresse2);
//    	personne.addAdresse(adresse3);
    	
    	Personne personne2 = new Personne();  
    	personne2.setId(2);
    	personne2.setNom("ninono"); 
    	personne2.setPrenom("Padra");
    	personne2.setAdresse(adresse1);
    	   	
    	Configuration configuration = new Configuration().configure(); 
    	SessionFactory sessionFactory = configuration.buildSessionFactory(); 
    	Session session = sessionFactory.openSession(); 
    	Transaction transaction = session.beginTransaction(); 
    	
    	session.persist(personne);
    	session.persist(personne2);
    	
    	transaction.commit(); 
    	session.close(); 
    	sessionFactory.close();

    }
}
